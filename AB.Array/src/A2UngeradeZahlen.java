
public class A2UngeradeZahlen {

	public static void main(String[] args) {
		
		int[] zahlen = new int[10]; // { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		for ( int i = 0; i < zahlen.length; i++) {
			zahlen [i] = i * 2 + 1;
		}
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print((zahlen[i] + " "));
		}
	
		
	}

}
