import java.util.*;

public class Buch implements Comparable<Buch>{
	
	private String autor;
	private String titel;
	private String isbn;
	
	public Buch (String autor, String titel, String ISBN) {
		this.autor = autor;
		this.titel = titel;
		this.isbn = ISBN;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		isbn = isbn;
	}
	
	@Override
	public int compareTo(Buch b) {
		return isbn.compareTo(b.getIsbn());
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj instanceof Buch) {
			Buch b = (Buch) obj;	
			return this.isbn.equals(b.getIsbn()); 
	    }
	    return false;
	}

	@Override
	public String toString() {
		return "Buch [Autor:" + autor + ", Titel:" + titel + ", ISBN:" + isbn + "]";
	}
	
	
	
	
	
	

}
