
public class Angestellter {

	private String name;
	private int gehalt;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;

	}

	public int getGehalt() {
		return gehalt;
	}

	public void setGehalt(int gehalt) {
		this.gehalt = gehalt;

	}

	public Angestellter(String name, int gehalt) {
		setName (name);
		this.gehalt = gehalt;
	}

}
