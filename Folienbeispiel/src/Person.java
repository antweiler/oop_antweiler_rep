
public class Person {
	
	private String vorname;
	private String nachname;
	private Datum geburtsdatum;
	private int personalnr;
	public static int anzahl = 0;
	private static int nextNumber = 1000;
	
	
	
	public Person(String vorname, String nachname, Datum geburtsdatum) {
		
		setPersonalnr(1000+anzahl);
		this.nachname = nachname;
		this.vorname = vorname;
		this.geburtsdatum = geburtsdatum;
		anzahl++;
		nextNumber++;
	}
	
	@Override
	public String toString() {
		return "\n Nr.: " + personalnr + " Vorname: " + vorname + " , Nachname: " + nachname + " , Geburtsdatum: " + getDatum();
	}
	
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public String getNachname() {
		return nachname;
	}
	
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getVorname() {
		return vorname;
	}
	
	public void setGeburtsdatum(Datum geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}
	
	public Datum getDatum() {
		return geburtsdatum;
	}
	
	public void setPersonalnr(int nextNumber) {
		this.personalnr = nextNumber;
	}
	
	public int getAnzahl() {
		return anzahl;
	}
	
	public static int getNextNumber() {
		return nextNumber;
	}
}
