
public class DruckerwarteschlangeTest {

	public static void main(String[] args) {
		Druckerwarteschlange Dsch = new Druckerwarteschlange(4);
		
		try {
			Dsch.addJob("Hallo");
			//Warteschlange voll, jetzt soll Exception kommen
			Dsch.addJob("Weiter");
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// Arbeite Warteschlange ab
		System.out.println(Dsch.nextJob());
		System.out.println(Dsch.nextJob());
		try {
			// Jetzt ist Speicher leer, sollte also wieder gehen 
			Dsch.addJob("Weiter");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(Dsch.nextJob());
	}
}