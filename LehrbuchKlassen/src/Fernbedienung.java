
public class Fernbedienung {

	private int senderspeichernr = 0;
	private String[] programme;

	// Konstruktor

	public Fernbedienung(int numPrograms) {
		this.programme = new String[numPrograms];
	}

	// Initialisiere angegebene Anzahl an Programmen
	public void senderwechsel() {
		for (int i = 0; i < senderspeichernr; i++) {
			this.programme[i] = "Programm " + (i + 1);
		}
		this.senderspeichernr = 0;
	}
	// Methode Senderwechsel

	public void nextProgram() {
		if (senderspeichernr < programme.length - 1) {
			this.senderspeichernr += 1;
		} else {
			this.senderspeichernr = 0;
		}
	}

	// Methode zur Benennung des aktuellen Programms

	public void setsenderspeichern(String name) {
		programme[senderspeichernr] = name;
	}

	// Methode um aktuelles Programm mit Sendernr auszugeben

	public void printSenderName() {
		System.out.println(" Sendernummer: " + senderspeichernr);
		System.out.println("Programm: " + programme[senderspeichernr]);
		System.out.println();
	}
}
