
public class StepCounter {
	
	private String datum;
	private int schrittzaehler = 0;
	
	private static int nextNumber = 0;
	
	public StepCounter (String datum) {
		setSchrittzaehler(nextNumber);
		setDatum(datum);
		
		nextNumber++;
	}
	
	
	public String getDatum() {
		return datum;
	}


	public void setDatum(String datum) {
		this.datum = datum;
	}


	public int getSchrittzaehler() {
		return schrittzaehler;
	}


	public void setSchrittzaehler(int schrittzaehler) {
		this.schrittzaehler = schrittzaehler;
	}


	public String toString() {
		return "Am " + datum + " bin ich " + schrittzaehler + " Schritte gegangen. ";
	}

}
