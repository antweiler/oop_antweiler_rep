
public class ShoppingCart{
	
	private Article[] cart;

	//Konstruktor
	public ShoppingCart() {
		
		this.cart = new Article[0];
	}

	public void addToCart(Article article) {
		//Vergr��ere das Array um 1 Element
		Article[] cartNew = new Article[cart.length + 1];
		//Kopiere alle Artikel r�ber
		for (int i = 0; i < cart.length; i++) {
			cartNew[i] = cart[i];
		}
		
		//Setze neuen Artikel ans Ende der Liste
		cartNew[cartNew.length - 1] = article;
		
		// �bernehme neue Liste
		cart = cartNew;
	}
	
	//Methode
	
	public void showBill() {
		//Gesamtpreis
		float sum = 0.0f;
		//Jeden Artikel durchgehen
		for (int i = 0; i < cart.length; i++) {
			Article article = cart[i];
			//Gebe Namen und Preis aus
			System.out.println(article + "\t " + article.getPrice() + "Euro");
			//Addiere zu Gesamtpreis
			sum = sum + article.getPrice();
			}
		System.out.println("------------------------------");
		//Gebe Gesamtpreis aus
		System.out.println("Gesamtpreis: " + sum + " Euro");
	}
	
	
	
	
	

}
