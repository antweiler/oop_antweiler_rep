
public class ShoppingCartTest {

	public static void main(String[] args) {

		Book b1 = new Book(122767676, 329, "Luigi Lo Iacono", "Websockets", 2015);
		DVD d1 = new DVD(12345432, 458, "Spiel mir das Lied vom Tod", "99:12",1);
		DVD d2 = new DVD(12423421, 567, "Casablanca, Classic Collection", "99:12", 1);
		
		
		

		System.out.println(b1);
		System.out.println(d1);
		System.out.println(d2);
		
		ShoppingCart wk = new ShoppingCart();
		wk.addToCart(b1);
		wk.addToCart(d1);
		wk.addToCart(d2);
		wk.showBill();
		//System.out.println(d2.showBill());
	}
}
