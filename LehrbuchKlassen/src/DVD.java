
public class DVD extends Article{

	public static final float VAT = 0.19f;
	private String name;
	private String duration;
	private int countryCode;
	
	
	

	public DVD(int articleNumber, float price, String name, String duration, int countryCode) {
		
		
		
		super(articleNumber, price);
		this.name = name;
		this.duration = duration;
		this.countryCode = countryCode;
	}
	
	public float GetPrice() {
		return (super.getPrice() + super.getPrice() * DVD.VAT * 100) / 100;
	}




	@Override
	public String toString() {
		return "DVD [" + name + "]\t";
	}
	
	
	
	
}
