
public class Book extends Article {

	public static final float VAT = 0.07f;
	private String author;
	private String title;
	private int year;

	// Konstruktor
	public Book(int articleNumber, float price, String author, String title, int year) {
		super(articleNumber, price);
		this.author = author;
		this.title = title;
		this.year = year;

	}
	
	public float GetPrice() {
		return (super.getPrice() + super.getPrice() * Book.VAT * 100) / 100;
	}

	



	@Override
	public String toString() {
		return "Book  [" + author + ", " + title + ", " + year + "]";
	}
	
	

}
