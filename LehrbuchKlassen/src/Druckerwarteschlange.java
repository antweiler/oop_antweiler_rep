
public class Druckerwarteschlange {
	private String[] jobs;
	private int nextSlot;
	private int nextJob;
	private int numJobs;
	private int maxJobs;
	// private int Exception;

	public Druckerwarteschlange(int maxJobs) {
		//public Person(String name, int age){
			//this.name = name;
			//this.age = age;
		
		jobs = new String[maxJobs];
		nextSlot = 0;
		nextJob = 0;
		numJobs = 0;
		this.maxJobs = maxJobs;

	}

	public void addJob(String job) throws Exception {
		if (numJobs >= maxJobs) {
			throw new Exception("Exception: Number of Jobs exceeded!");
		}

		jobs[nextSlot] = job;
		// Sorge daf�r, dass n�chste Position immer im Array-Bereich bleibt
		nextSlot = (nextSlot + 1) % maxJobs;
		numJobs = numJobs + 1;

	}

	public String nextJob() {
		String job = null;
		if (numJobs > 0) {
			numJobs = numJobs - 1;
			job = jobs[nextJob];
			// bestimme n�chste Jobnr
			nextJob = (nextJob + 1) % maxJobs;

		}
		return job;
	}

}
