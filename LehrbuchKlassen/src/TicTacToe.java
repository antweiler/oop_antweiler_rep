
public class TicTacToe {
	
	private int[]field;
	private int mark;
	
	//Konstruktor, der das Spielfeld initialisiert
	public TicTacToe() {
		field = new int[9];
		reset();
	}
	// Methode, die alle Felder eines Spielfeldes mit 0-Werten initialisiert
	
	public void reset() {
		//Gehe alle Felder durch und l�sche Inhalt
		for (int i = 0; i < field.length; i++) {
			field[i] = 0;
		}
		// Setze Startsymbol
		mark = 1;
	}
	
	// Methode zum Setzen einer Marke. Die Methode erh�lt die x,y-Koordinate und setzt die Marke.
	//Wenn die Voraussetzungen dies erm�glichen, wird 'true'zur�ckgegeben, ansonsten 'false'.
	public boolean setMarke(int x, int y) {
		// nur g�ltige Spielfeldgr��en akzeptieren
		if (x < 0 || x > 2 || y < 0 || y > 2) {
			return false;
		}
		
		//Bestimme Position im Array
		int pos = 3 * y + x;
		
		// Feld schon belegt? Dann aus Funktion springen
		if (field[pos] > 0) {
			return false;
		}
		
		// Sonst setze Markierung an Position
		field[pos] = mark;
		
		// Setze neues Zeichen (0 oder X)
		mark = (mark % 2) + 1;
		
		return true;
	}
	
	// Methoden zum Generieren der Ausgabe des Spielfelds.
	// Die Methode bekommt keine Parameter �bergeben und liefert einen String mit der Repr�sentation des Spielfeldes zur�ck.
	public String toString() {
		String temp = "";
		
		// Gehe Spielfeld durch
		for (int i = 0; i < field.length; i++) {
			switch (field[i]) {
			case 0:
				temp = temp + " ";
				break;
			case 1: 
				temp = temp + "X";
				break;
			case 2: 
				temp = temp + "O";
				break;
			default:
				temp = temp + " ";
			}
			// Wenn aktuelle Feldnummer nicht durch 3 teilbar ist, Spalte malen
			if ((i + 1) % 3 != 0) {
				temp = temp + "|";
			}
			// nach 3 Elementen neue Zeile
			if ((i + 1) % 3 == 0 && i < 6) {
				temp = temp + "\n";
				temp = temp + "-+-+-";
				temp = temp + "\n";
			}
		}
		
		// Gebe Spielfeld mit Zeilenumbruch zur�ck
		return temp = temp + "\n";
	}
}
