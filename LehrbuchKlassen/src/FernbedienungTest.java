
public class FernbedienungTest {

	public static void main(String[] args) {


		Fernbedienung fbg = new Fernbedienung(5);
		fbg.setsenderspeichern("ARD");
		fbg.printSenderName();
		
		//gehe 3 Sender weiter und gebe jeden aus
		for (int i = 0; i < 3; i++) {
			fbg.nextProgram();
		}
		
		//Setze Sendername
		fbg.setsenderspeichern("RTL");
		fbg.printSenderName();
		
		//Gehe 6 Mal nach vorne
		for (int i = 0; i < 7; i++) {
			fbg.nextProgram();
			fbg.printSenderName();
		}

	}

}
