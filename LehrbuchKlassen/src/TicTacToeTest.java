
public class TicTacToeTest {

	public static void main(String[] args) {
		TicTacToe t = new TicTacToe();
		
		System.out.println(t);
		
		t.setMarke(2, 2);
		System.out.println(t);
		
		t.setMarke(2, 0);
		System.out.println("O" + t);
		
		t.setMarke(1, 1);
		System.out.println(t);
		
		t.setMarke(0, 0);
		System.out.println(t);
		
		t.setMarke(1, 2);
		System.out.println(t);
		
		t.setMarke(0, 1);
		System.out.println(t);

		t.setMarke(0, 2);
		System.out.println(t);
	}

}
