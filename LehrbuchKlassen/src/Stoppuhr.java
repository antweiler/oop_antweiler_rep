
public class Stoppuhr {

	private long startzeit;
	private long stoppzeit;

	// wird gerade Zeit gestoppt
	private boolean running;

	// Konstruktor

	public Stoppuhr() {
		this.startzeit = 0;
		this.stoppzeit = 0;
		this.running = false;
	}

	// Methode zum Starten eines Vorgangs
	public void start() {
		if (this.running == false) {
			this.startzeit = System.currentTimeMillis();
			this.running = true;
		}

	}

	// Methode zum Stoppen eines Vorgangs
	public void stop() {
		if (this.running == true) {
			this.stoppzeit = System.currentTimeMillis();
			this.running = false;
		}

	}
	
	// Methode zum Berechnen der vergangenen Zeit - Ergebnis als 
	// String zur�ckgegebe
	public String elapsedTime() {
		long time;
		if (this.running == true) {
			// Zeit l�uft noch 
			// nehme aktuelle Zeit
			time = System.currentTimeMillis() - this.startzeit;
		}
		else {
			// Zeit l�uft nicht (mehr)
			// nehme gestoppte Zeit
			time = this.stoppzeit -  this.startzeit;
		}
		
		// Bestimme Sekunden und Hundertstel
		long seconds = time / 1000;
		long hundreds = time % 1000;
		
		// Gebe Zeit formatiert aus
		return "" + seconds + "." + hundreds;
	}
}
