
public class Exceptionfyear extends Exception {

	private int fyearwert;

	public Exceptionfyear(String msg,int fyearwert) {
		super(msg);
		this.fyearwert = fyearwert;
	}

	public int getFyearwert() {
		return fyearwert;
	}
}
