
public class Aufgabe1 extends Exception{

	public static void main(String[] args) {
		
		//System.out.println(div(5,0));
		
		try {
			
			int ergebnis = div(33,11);
			System.out.println(ergebnis);
		}
		catch(Exception e) {
			System.out.println("Der Nenner darf nicht 0 sein!");
			
		}
		
		
	}
	
	public static int div(int wert1, int wert2) {
		return wert1 / wert2;
	}

}
