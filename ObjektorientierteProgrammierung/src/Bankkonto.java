
public class Bankkonto {

	private String inhaber;
	private int kontonr;
	private double kstand;

	// Konstruktor
	public Bankkonto(String inhaber, int kontonr, double kstand) {
		super();
		this.inhaber = inhaber;
	    this.kontonr = kontonr;
		this.kstand = kstand;
	}
	
	@Override
	public String toString() {
		return "( " + inhaber + ", " + kontonr + ", " + kstand + ")";
	}

	// Methoden
	public String getInhaber() {
		return inhaber;
	}

	public void setInhaber(String inhaber) {
		this.inhaber = inhaber;
	}

	public int getKontonr() {
		return kontonr;
	}

	public void setKontonr(int kontonr) {
		this.kontonr = kontonr;
	}

	public int getKstand() {
		return kstand;
	}

	public void setKstand(int kstand) {
		this.kstand = kstand;
	}

	// Methoden

	public void einzahlenBetrag(double betrag) {
		kstand += betrag;
		return betrag;

	}
	
	public double auszahlenBetrag(double betrag) {
		kstand -= betrag;
		return betrag;	// Kontrolle, dass nicht zuviel ausgezahlt wird, als vorhanden

	}
	
	@Override
	public String toString() {
		return this.inhaber + "   Kontonr.: " + this.kontonr + "   Kontostand: " + this.kstand;
	}
	
	

}
