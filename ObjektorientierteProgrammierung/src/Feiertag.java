
public class Feiertag extends Datum{
	
	private String name;
	
	//Konstruktor
	public Feiertag(int tag, int monat, int jahr, String name) {
		super(tag, monat, jahr);
		this.name = name;
	}
	
	// Verwaltungsmethoden
	public String getName() { 
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override 
	public String toString() {
		return " Datum: " + this.getTag() + "." + this.getMonat() +"."+ this.getJahr() + "  Feiertag: " + this.name  ;
	}
}