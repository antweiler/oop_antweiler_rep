
public class Bankkontotest {

	public static void main(String[] args) {
		
		Bankkonto b1 = new Bankkonto("Wunder", 1234, 4321.0);
		Dispokonto d1 = new Dispokonto("Max Mustermann", 12345, 700.0, 1000);
		
		//System.out.println(b1);
		
		//k1.einzahlen(250.0);
		
		//System.out.println(k1.auszahlen(340.0));
		
		//System.out.println(b1);
		
		Bankkonto[] kontoliste = new Bankkonto[3];
		
		kontoliste[0] = b1;
		kontoliste[1] = d1;
		kontoliste[2] = new Bankkonto("Musterfrau", 2347, 200);
		
		for (Bankkonto k : kontoliste)
			System.out.println(k);		// Superklasse
	}

}
