
public class Dispokonto extends Bankkonto {
	
	private double dispo;
	
	//Konstruktoren
	public Dispokonto (String inhaber, int kontonr, double kstand,double dispo) {
		super(inhaber, kontonr, kstand);
		this.dispo = dispo;
	}
	
	//Methoden
	public double getDispokonto() {
		return dispo;
	}

	public void setDispokonto(double dispo) {
		this.dispo = dispo;
	}
	
	@Override
	public String toString() {
		return this.getInhaber() + "   Kontonr.: " + this.getKontonr() + "   Kontostand: " + this.getKstand() +  " Dispo: " + this.getDispokonto();
	}
	
	@Override
	public double auszahlen(double betrag) {
		double neuerkstand = getKstand() - betrag;
		if (neuerkstand >= -1 * dispo)
			setkstand(neuerkstand);
			return betrag;
		}
	    betrag = getKstnd + dispo;
	    setKstand(-1 * dispo);
	    return betrag;						//Kstand = 700; dispo = 1000; neuerkstand = -500 //auszahlen(1200)
	    									//auszahlen(600) danach: -1100 ; 500 ausweisen
	 @Override
	 public String toString() {
		return super.toString()	+ ", "+ dispo;
		 //return this.getInhaber() + "   Kontonr.: " + this.getKontonr() + "   Kontostand: " + this.getKstand() +  " Dispo: " + this.getDispo();
		}
		
	}

}
