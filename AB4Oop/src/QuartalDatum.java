
public class QuartalDatum {

	public class Datum {

		private int tag;
		private int monat;
		private int jahr;

		public Datum() {
			this.tag = 1;
			this.monat = 1;
			this.jahr = 1970;
		}

		public Datum(int tag, int monat, int jahr) {
			setTag(tag);
			setMonat(monat);
			setJahr(jahr);
		}

		public int getTag() {
			return this.tag;
		}

		public void setTag(int tag) {
			this.tag = tag;
		}

		public int getMonat() {
			return monat;
		}

		public void setMonat(int monat) {
			// if (alter > 0)
			// this.alter = alter;
		}

		public int getJahr() {
			return this.jahr;
		}

		public void setJahr(int jahr) {
			this.jahr = jahr;
		}

		public String toString() {  
			return "[ Datum: " + this.tag + "." + this.monat + "." + this.jahr +"]";
		}
	}

		public static (int) rechneQuartal (){
			if (int monat < 4)
				return 1;
		}
}
