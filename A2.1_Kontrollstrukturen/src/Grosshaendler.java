import java.util.Scanner;

public class Grosshaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Geben Sie bitte Liefermenge ein:");
		int liefermenge = myScanner.nextInt();

		System.out.println("Geben Sie bitte den Einzelpreis ein:");
		double einzelpreis = myScanner.nextDouble();

		if (liefermenge >= 10) {
			System.out.println("Ihre Rechnung betr�gt " + liefermenge * einzelpreis * 1.19 + " �");
		} else {
			System.out.printf("Ihre Rechnung betr�gt %.2f  � inklusive 10� Lieferpauschale.", (liefermenge * einzelpreis * 1.19+ 10));
		}
	}

}
