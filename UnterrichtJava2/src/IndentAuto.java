
public class IndentAuto {
	private String kennzeichen;
//	private String hersteller;
	private double preis;

	public IndentAuto(String kennzeichen, double preis) {
		// this.kennzeichen = kennzeichen;
		setKennzeichen(kennzeichen);
		setPreis(preis);
	}

//	public IndentAuto (String hersteller) {
	// this.hersteller = hersteller;
	// }

	public IndentAuto(Double preis) {
		this.preis = preis;
	}

	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}

	// public void setHersteller (string hersteller) {
	// this.hersteller = hersteller;
	// }

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public double getPreis() {
		return this.preis;
	}

	public String getKennzeichen() {
		return this.kennzeichen;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IndentAuto) {
			IndentAuto at = (IndentAuto) obj;
			//if (this.kennzeichen.equals(at.getKennzeichen())) {
			//	return true;
			//	else
			//		return false;
			return this.kennzeichen.equals(at.getKennzeichen());
			}
			else
				return false;
		}
	
	// public String getHerstller () {
	// return this.hersteller;
	// }

	@Override
	public String toString() {
		return "Auto(Kennzeichen: " + this.kennzeichen + ", Preis: " + this.preis + ")";
	}
}
