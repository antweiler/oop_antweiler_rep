
public class Person {
	
	private String vorname;
	private String nachname;
	private Datum geburtsdatum;
	
	
	
	public Person(String vorname, String nachname, Datum geburtsdatum) {
		this.nachname = nachname;
		this.vorname = vorname;
		this.geburtsdatum = geburtsdatum;
	}
	
	@Override
	public String toString() {
		return " Vorname: " + vorname + " , Nachname: " + nachname + " , Geburtsdatum: " + getDatum();
	}
	
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public String getNachname() {
		return nachname;
	}
	
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getVorname() {
		return vorname;
	}
	
	public void setGeburtsdatum(Datum geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}
	
	public Datum getDatum() {
		return geburtsdatum;
	}
	
	
}
