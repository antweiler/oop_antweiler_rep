
public class Buch {

	private int buchnummer;
	private String titel;
	
	private static int nextNumber = 1000;
	
	public Buch(String titel) {
		int buchnummer = 1000;
		setBuchnummer(nextNumber);
		setTitel(titel);
		
		nextNumber++;
	}
	
	public static int getNextNumber() {
		return nextNumber;
	}

	public int getBuchnummer() {
		return buchnummer;
	}

	public void setBuchnummer(int buchnummer) {
		this.buchnummer = buchnummer;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}
	
	@Override
	public String toString() {
		return "(Buchnummer: " + buchnummer + ", Titel: " + titel + ")"; 
	}
}
